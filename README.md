# git_bez_razmisljanja or

![GITGUD](img/gitgud.jpeg?raw=true "Title")
### Git inicijalna konfiguracija 
Git requires ( on initialization ) email and username 

```$ git config --global user.email "none"```

```$ git config --global user.name "username"```

![GITovanje](img/git.jpeg?raw=true "Title")

### To enable colors 
```$ git config --global color.ui true ```

### Create new (sw/hw/etc) project 
```$ cd new_project```

### Git initialization in new project 
```man git | grep -n “init“```

### Create an empty Git repository or reinitialize an existing one.
```$ git init ```

### Using whatis command
```$ whatis git-add  ```

git-add (1)          - Add file contents to the index

Add all files under current directory ( . DOT notation) 
### NOTE use this only when starting new project 
```$ git add .```

### Permanently store the contents of the index in the repository with git commit:
```$ git commit ```

git-commit (1)       - Record changes to the repository

### Git add doesn’t output what is added to see what is added use:
```$ git status``` 

git-status (1)       - Show the working tree status

### To add new files: 
```$ git add file1 file2 file3 ```

### Modify file1 
Then add that file to be staged 
```$ git add file1```

### To see what is modified use:
```$ git diff –cached ``` 

git-diff (1)         - Show changes between commits

### Git commit
``` $ git commit or $ git commit -a``` 

automatically notice any modified (but not new) files, add them to the index, and commit, all in one step.

## Istorija i logovi
```$ git log ```

git-log (1)          - Show commit logs


### Same as git diff 
```$ git log -p ```

### History of specific file 
```$ git log -p --path/to/file```



### To see what is changed where and how much in size 
```$ git log --stat```


### Git branches 
```$ whatis git-branch ```

git-branch (1)       - List, create, or delete branches

```$ man git-branch ```

### Creating new branch of development 

```$ git branch branch_name_here ```


### List available branches
```$ git branch ``` 


### Switch branch 
```$ git branch ```
### Or list branches 
```$ git checkout branch_name_here ```

git-checkout (1)     - Switch branches or restore working tree files

* has moved to the working branch 
```$ git branch ```


### Edit file1 and commit 
```$ git commit -a ```

### Merge branches 
NOTE be sure you switch to the right branch 
```$ git merge branch_name_here ```


## Conflict resolution 

![GITGUD](img/roll.gif?raw=true "Title")


When you merge branches that contain changes in same file it will result in conflict when running 

```$ git merge branch_name_here``` 

### To see where is conflict use 
```$ git diff ```

### Rezolucija konflikata pomocu meld-a  ( f*k vimdiff ) 

### List available mergetools 
```$ git mergetool --tool-help ```

### Use tool from currently available tools 
```$ git mergetool -t vimdiff3 ```   
( or meld for graphical ) 


### Graphical commit and merge history 
```$ gitk ``` 

### Deleting experimental branch 
```$ git branch -d branch_name_here```

Command ensures that the changes in the experimental branch are already in the current branch

git-reset (1)        - Reset current HEAD to the specified state

usage 

```$ git add * ```

now if you included files by accident you can reset add command by using reset

```$ git reset . ```


## Git ignorisi 
https://git-scm.com/docs/gitignore
local and global 

### Local
If you create a file in your repository named .gitignore, Git uses it to determine which files and directories to ignore, before you make a commit.

```$ touch .gitignore ```

### Global
You can also create a global .gitignore file, which is a list of rules for ignoring files in every Git repository on your computer. For example, you might create the file at ~/.gitignore_global and add some rules to it.

```$ git config --global core.excludesfile ~/.gitignore_global```

note pattern matching 

### Excluding directories 

dirname/
.dirname/
 /dirname/*

exclude files 

PetaoKlasa.cc
*.cc
*.html

### Komentari  
\# some comment in .gitignore


### update git repo 
$ git pull origin master
